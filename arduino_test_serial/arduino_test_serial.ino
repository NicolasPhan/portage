
int btnPin  = 8;
int ledPin[3]  = {5, 6, 7};
int done    = 0;
int burst   = 1;

bool   ledState = LOW;
long  timestamp = 0;
long  duration = 20; // ms

int   cur_tty = 1;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(btnPin, INPUT);
  pinMode(ledPin[0], OUTPUT);
  pinMode(ledPin[1], OUTPUT);
  pinMode(ledPin[2], OUTPUT);
  randomSeed(analogRead(0));
}

void loop() {
  int     btnVal;
  char    c;
  String  s;

  /* WRITE on button push */
  btnVal = digitalRead(btnPin);
  
  if ((done == 0 || burst) && btnVal) {
    // Select TTY randomly
    c = random(0, 3) + '0';
    Serial.print(c);

    // Print random char
    c = random(0, 26) + 'a';
    Serial.print(c);
    
    done = 1;
  }

  if (done == 1 and not btnVal) {
    done = 0;
  }

  /* READ and turn led on */
  if (ledState == LOW and Serial.available()) {
    c = Serial.read();
    timestamp = millis();
    if (c >= 'A' and c <= 'C') {
      cur_tty = c - 'A';
    }
    ledState = HIGH;
    digitalWrite(ledPin[cur_tty], ledState);
  }

  /* Turn off the led after 'duration' ms */
  if (ledState == HIGH and millis() - timestamp > duration) {
    ledState = LOW;
    digitalWrite(ledPin[cur_tty], ledState);
  }
}
