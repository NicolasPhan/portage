#!/usr/bin/python

import os
import sys
import signal
import serial
import socket
import select

def print_usage() :
    print('Usage:     ./ttyalmos.py <port> [<nb_ttys>]');
    print('Examples:');
    print('  ./ttyalmos.py /dev/ttyACM0 2');
    print('  ./ttyalmos.py /dev/ttyACM1');
    print('Notes:');
    print('  N.B. nb_ttys = 1 by default');

print('TTY Multiplexer for ALMOS-MKH');

def check_args_length() :
    if len(sys.argv) < 2 :
        print_usage()
        exit();

def get_nb_ttys() :
    if len(sys.argv) == 3 :
        nb_ttys = int(sys.argv[2])
        if nb_ttys < 1 :
            print('Too many ttys');
            exit()
    else :
        nb_ttys = 1;
    return nb_ttys

check_args_length()
nb_ttys = get_nb_ttys()

# Connect to serial port
port = sys.argv[1]
ser = serial.Serial(port)

## Creating the xterms
inputs  = []    # Objects to listen to with select()
servers = []    # List of sockets used by the server to communicate with all the xterms
pids    = []    # List of pid of the children (the xterms)

inputs.append(ser)

# ---- Forking the n xterms
for i in range(nb_ttys) :
    client, server = socket.socketpair();
    pid = os.fork()
    if (pid == 0) :
        break;
    else :
        servers.append(server);
        inputs.append(server);
        pids.append(pid);
        client.close();

## Children (= clients) - Launching the xterm
if pid == 0 :
    server.close();
    args = "-S0/" + str(client.fileno());
    os.execlp("xterm", "xterm", args,
            "-T", "TTY " + str(i));

# ---- Parent (= server) - Main loop
for i in range(nb_ttys) :
    servers[i].send("Hello " + str(i));

try :
    while True :
        readable, writable, exceptional = select.select(inputs, [], []);

        for r in readable :
            # Redirect chars from the serial port to the correct tty.
            if r == ser :
                s = ser.read(2);
                print(s);
                destno = int(s[0]);
                if (destno < 0 or destno >= nb_ttys) :
                    print('Bad destno : ' + s[0])
                    exit()
                char = s[1];
                servers[destno].send(char);
            # Redirect chars from a tty to the serial, with the correct header
            else :
                for i in range(nb_ttys) :
                    if r == servers[i] :
                        break;
                s = servers[i].recv(1024)
                str2send = chr(i + ord('A')) + s;
                print(str2send)
                ser.write(str2send)
except KeyboardInterrupt :
    # Kill children
    for pid in pids :
        os.kill(pid, signal.SIGTERM);

    # Close everything
    client.close()
    server.close()
    ser.close()

